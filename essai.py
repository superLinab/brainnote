#coding=utf-8

from brainNote import *

ensNote = EnsembleNote()
ensNote.lireNotes()

print ensNote.chercherDansLensNote('marcher')

# class Objqq():
#     def __init__(self):
#         self.chat = "chat"
#         self.chien = "chien"

# objet = Objqq()
# objet2 = Objqq()

# # def serialiseur_perso(obj):
# #     if isinstance(obj, Playlist):
# #         return {"__class__": "Playlist",
# #                 "nom": obj.nom,
# #                 "musiques": obj.musiques}
# #     raise TypeError(repr(obj) + " n'est pas sérialisable !")

# # def deserialiseur_perso(obj_dict):
# #     if "__class__" in obj_dict:
# #         if obj_dict["__class__"] == "Playlist":
# #             obj = Playlist(objet["nom"])
# #             obj.musiques = objet["musiques"]
# #             return obj
# #     return objet


# # La ligne:
# #EN gros, faut donner un dictionnaire à json.dumps, pour qu'il puisse en faire un fichier json
# #Et donc, on peut lui donner une fonction qui transforme notre element en dico (on fait comme on veut hein :D)
# #grace à la ligne:
# # with open("MaPlaylist.json", "w", encoding="utf-8") as fichier:
# #     json.dump(playlist, fichier, default=serialiseur_perso)

# print json.dumps({"anise":"narsit", "naruist":3, "anrsiteanru": ["narsit", "narsit"]}, indent=4)
# # donnera:
# # {
# #     "anise": "narsit",
# #     "anrsiteanru": [
# #         "narsit",
# #         "narsit"
# #     ],
# #     "naruist": 3
# # }
