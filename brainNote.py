#coding:utf-8

# Copyright (C) 2015 Delhomme Fabien

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import os
import re

def comptageDiese(ligne):
    """
    Compte le nombre de # en début de ligne
    """
    c = 0
    if len(ligne) == 0:
        return 0
    while ligne[c]=='#':
        c+=1
    return c

class Feuille:
    def __init__(self, contenu=[], pere=None):
        self.contenu = contenu
        self.hashtags =[]
        self.pere = pere

    # def __repr__(self):
    #     return self.contenu

class Noeud:
    """
    Représente une un noeud, dont la racine + ses descendants représentent la note.
    C'est une classe interne, pas destinée à être utilisée par l'utilisateur.
    Elle sert seulement à implémenter un arbre pour pouvoir sturcturer les notes
    Autrement dit, le contenu d'une note n'est caractérisé que par la racine des son arbres
    """
    def __init__(self,titre, pere):
        self.titre = titre
        self.feuille = []  # Cela correspond aux paragraphes contenus dans ce noeud, on va stocker l'adresse de l'objet feuille correspondant.
        self.fils = []     # Tableau de "pointeur" Noeud fils
        self.hashtags = [] # Liste des hashtags

        if pere!=None:
            self.pere = pere
        else:
            self.pere = None   # Pere est de type Noeud ou None

    def ajouterFils(self, nouveauFils):
        assert isinstance(nouveauFils, Noeud)
        self.fils.append(nouveauFils)
        return nouveauFils

    def ajouterFeuille(self, nouvelleFeuille):
        assert isinstance(nouvelleFeuille, Feuille)
        self.feuille.append(nouvelleFeuille)

    def getPere(self):
        if self.pere != None:
            return self.pere
        else:
            return self
    #
    # def __repr__(self, niveau=0):
    #     retour = "\t"*niveau+repr(self.titre) + "\n" +"\t"*(niveau+1)+ repr(self.feuille)+"\n"
    #     for fils in self.fils:
    #         retour += fils.__repr__(niveau+1)
    #     return retour

    def hasher(self):
        """
        Retourne les hashtags de la note.
        """
        #Pour chaque feuille on récupére les hashtags
        ensHash = []

        # hashtags tel que  "bla bla bla -le roi soleil - bla bla.."
        for i in range(len(self.feuille)):
            ensHash+=re.findall("-((\w\s+)*-)", self.feuille[i])

        self.hashtags = list(set(ensHash))  #Passer par le set permet de rendre
                                            #les éléments uniques (la grosse
                                            #flemme de faire un dico pour ça
                                            #...)
        return self.hashtags

    def ensFeuille(self, courant):
        """
        À partir de self.racine, parcourt tout l'arbre en quête des feuilles 
        """
        tabEnsFeuille = courant.feuille
        for i in range(len(courant.fils)):
            tabEnsFeuille += self.ensFeuille(courant.fils[i])
        return tabEnsFeuille

class Constructeur:
    def construireEnsNotes(self):
        """
        Construit le fichier ensnotes.json contenant l'ensembles des notes.json
        """
        # TODO: optimiser le processus en ne reconstruisant que les notes modifiées.
        with open('ensnotes.json', 'w', encoding='utf-8') as f:
            json.dump(self.note, f, indent=4)
        exit(0)


class EnsembleNote:
    def __init__(self):
        self.ensembleDeNote = [] #Contient un ensemble de Note

    # A l'aide du fichier ensnotes.json, il lie (du verbe lier) tous les hashtags
    # avec les paragraphes

    def lireNotes(self):
        """
        Construit le fichier ensnotes.json qui listent l'ensemble des
        notes du répertoire courant et liste avec les hashtags et mots
        importants rencontrés

        """

        listeFichier = os.listdir('.') #Fais la liste des répertoires courants
        listeNotes=[]

        for i,v in enumerate(listeFichier): #On fait la liste des *.md
            if ".md" in v:
                listeNotes.append(listeFichier[i])

        for localisation in listeNotes:
            note = Note(localisation)
            note.chargerContenuNote()
            note.chargerHashTagsNote()
            self.ensembleDeNote.append(note)


    def chercherDansLensNote(self, motclef):
        occurences = []
        print self.ensembleDeNote

        for note in self.ensembleDeNote:
            if note.hashtags.has_key(motclef):
                occurences += note.localisation, [note.hashtags[motclef][2*i +1].contenu for i in range(len(note.hashtags[motclef])/2)]
        return occurences

class Note:
    "Représente une note "
    def __init__(self, localisation):
        self.racine = None
        self.localisation = localisation
        #TODO: faire une liste de hashtags de date, comme ça on pourra afficher une phrise chornologique de toute les notes :D
        self.hashtags = None

    def chargerContenuNote(self):
        """
        Lis une note dont l'emplacement est donnée en paramètre
        Retourne la racine de l'arbre de la note associée.
        """
        f = open(self.localisation)
        txt = f.read().split("\n")
        f.close()

        #On commence par la racine, on lui met un titre, et elle n'a pas de père.
        noeudCourant =  Noeud(txt[0], None) 

        nbDiese = 1                     # C'est un titre, donc on est en h1
        hauteurTitreCourante = 1

        for ligne in txt[1::]:          # Puisque grâce au split, chaque indice de txt correspond à la i-ieme ligne...
            nbDieseCourant = comptageDiese(ligne)

            #On ajoute dans le noeud courant

            # Si il n'y a pas de dièse, alors soit c'est un paragraphe,
            # soit c'est une ligne vide

            if nbDieseCourant == 0:

                # Si c'est une ligne vide, alors on fait une nouvelle feuille,
                # comme ça ce qui suit ira dans cette feuille.
                if ligne != '':
                    if noeudCourant.feuille== []:
                        noeudCourant.ajouterFeuille(Feuille(ligne,noeudCourant))
                    #Sinon, alors on ajoute ce qui suit dans la dernière feuille
                    else:
                        # TODO: changer noeudCourant au lieu de demander de se déplacer sur la dernière feuille (pb de perf ?)
                        noeudCourant.feuille[-1].contenu+="\n" + ligne
                if ligne == '': # and ligne d'après ne commence pas par '#'
                    noeudCourant.ajouterFeuille(Feuille("", noeudCourant))

            #Sinon, c'est un titre, donc trois cas de présentent:

            elif nbDieseCourant == hauteurTitreCourante:
                #On se déplace horizontalement
                nouveauFils = Noeud(ligne, noeudCourant.pere)
                noeudCourant = noeudCourant.pere.ajouterFils(nouveauFils)

            elif nbDieseCourant > hauteurTitreCourante:
                #On cree un nouveau fils avec le titre qui va bien:
                nouveauFils = Noeud(ligne, noeudCourant)
                noeudCourant = noeudCourant.ajouterFils(nouveauFils)
                hauteurTitreCourante = nbDieseCourant

            #Sinon, il faut remonter du nombre de différence de dièse + 1
            else: #Alors, nbDieseCourant < hauteurTitreCourante, c'est donc un saut, il faut remonter !
                    # + 2 car +1 à cause du fait qu'il faut remonter une fois de plus, et +1 à cause du range
                for i in range(hauteurTitreCourante - nbDieseCourant + 2):
                    noeudCourant = noeudCourant.getPere()

                nouveauNoeud = Noeud(ligne, pere=noeudCourant)
                noeudCourant = noeudCourant.ajouterFils(nouveauNoeud)
                hauteurTitreCourante=nbDieseCourant

            nbDiese = nbDieseCourant

        #On remonte, car noeudCourant correspond au dernier noeud/feuille ajouté(e): (on veut la racine)
        # Plutôt rapide, vu que c'est en O(profondeurTitre) donc au max 4 ou 5 itérations.
        while noeudCourant.pere != None:
            noeudCourant = noeudCourant.pere

        #On met la racine dans self.racine:

        self.racine = noeudCourant

    def chargerHashTagsNote(self):
        """
        Si le contenu de la note est chargée, charger les hashtags !
        """

        feuilles = self.racine.ensFeuille(self.racine)
        ensHash = {}

        pattern = re.compile("-([\w\s?]*)-")
        for i in range(len(feuilles)):
            occurenceDeHash = pattern.findall(feuilles[i].contenu)
            if len(occurenceDeHash) != 0:
            #On veut associer à chaque hashtags, le titre est le paragraphe dans leque il est présent !
                for j in range(len(occurenceDeHash)):
                    if ensHash.has_key(occurenceDeHash[j]):
                        ensHash[occurenceDeHash[j]]+=(feuilles[i].pere.titre, feuilles[i])
                    else:
                        ensHash[occurenceDeHash[j]] = (feuilles[i].pere.titre, feuilles[i])
        self.hashtags = ensHash
