#Brain Note

Bienvenue sur l’application 100% française, toujours en phase beta,
qui permettra de prendre des notes efficacement !

Le programme a pour but d’être:

    - Facile à comprendre, et à modifier (fait en python)
    - Plutôt rapide (essai d’éviter les lectures de fichier inutiles, en cours !)
    - Se baser sur une prise de note simple: J’ai choisi le markdown qui est le langage le plus simple à comprendre est le plus répandu, et très lisible.
    - Multiplateforme
    - Facilement exportable (grâce à des outils comme pandoc)

N’hésitez pas à participer au projet !!! Je suis encore un apprenti et
je suis **ouvert à toutes critiques constructives **.

##But

Le but est de créer un système qui permette de **prendre des notes de façon
linéaire**, et d'ensuite regrouper toutes les notes et les concepts et de les *transformer* dans une carte **non linéaire** (à la manière d’une carte mentale).
Les notes prises pourront servir entres autres pour:

    - la lecture d'un livre,
    - une leçon,
    - un matière,
    - un sujet,
    - un exposé
    - ...

##Fonctionnement

Le programme lit toutes les notes dans un format **markdown**. Pour
chaque note, il crée un fichier du même nom au format json, qui résume
la note.

Puis, il crée un fichier qui résume tout... (utile ?) Ensuite, il en déduit
le lien entre chaque notes grâce aux hashtags (ceux avec un dièse) et aux mots importants (ceux entourés d’astérix).

Ensuite, il suffit de taper un mot clé, un catégorie ou un sujet, pour
obtenir toutes les notes qui lui sont relatives. Le programme pourra
produire une carte mentale interactive en mettant le sujet saisi au
centre d’une fenêtre.

## Fonctionnalitée:

- Programmé en python
- Comprend le markdown
- Permet de répertorier vos notes dynamiquement par **themes** (hashtags).

##Structure et objets

L'ensemble de note(EnsembleDeNote) est constitué d'un ensemble de note, chaque note
est constituée d'article, et chaque article est constitué de
paragraphe, qui contiennent chacuns des hashtags qui donnent la
catégorie de l'article, et le contenu en lui même du paragraphe, plus
quelque mot en gras par exemple.

NB: le **hashtag** (en dièse) donne la **categorie** de l'article ou du
paragraphe, et les mots en **gras** donnent juste les **mots
importants**.

Résumé:

* Ensemble de note:
    * note1:
        *article1
            * paragraphe1 ##hashtag1, ##hashtag2...
            * paragraphe2 ##hashtag3, ##hashtag2
        *article2
        *article3
            *pararagraphe4 ##hashtag2
    * note2
    * note3
    * note4
    * ....

Et deviendra:

```
    hashtag1 -> note1/article1/paragraphe1
    hashtag2 -> note1/article2/paragraphe2
             -> note1/article3/pararagraphe4
    hashtag3 -> ...
```

Pour la structure d’une note, voir plus bas.

Pour les **mots importants**, il suffit de faire une rapide recherche,
montrant tous les **paragraphes ayant ces mots importants**. On pourra
faire un tri sur le nombre d'occurrences trouvées. 

####Structure d'une note.js

```
#!json
    {
        “titre”: "titre",
        “hashtags”: {
            "esasi",
            "deuxièmeHashTag"
        }
        “paragraphe”:[
            "titre1": {
                "positionduParagrapheauTitre1",
                "motimportant1",
                "motimportant2",
                "motimportant3"
            },
            "titre2": "..."
        ]
    }
```

##Limites:

Les limites actuelles dans la conception du script:

    - Le programme ne reconnaît que deux niveaux, les h1, et les h2. Cela devrait être suffisant.
    - Comment traiter les paragraphes qui ne font « que » une ligne ou deux ?
    - Comment traiter les paragraphes sans hashtags ?
