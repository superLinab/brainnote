#coding:utf-8

#Lecture d'une note
#Codage d'un arbre:
import re

class Noeud():
    def __init__(self,titre, pere):
        self.titre = titre
        self.feuille = []  # Cela correspond aux paragraphes contenus dans ce noeud
        self.fils = []     # Tableau de "pointeur" Noeud fils
        self.hashtags = [] # Liste des hashtags

        if pere!=None:
            self.pere = pere
        else:
            self.pere = None   # Pere est de type Noeud ou None

    def ajouterFils(self, nouveauFils):
        assert isinstance(nouveauFils, Noeud)
        self.fils.append(nouveauFils)
        return nouveauFils

    def ajouterFeuille(self, nouvelleFeuille):
        self.feuille.append(nouvelleFeuille)

    def getPere(self):
        if self.pere != None:
            return self.pere
        else:
            return self

    def __repr__(self, niveau=0):
        retour = "\t"*niveau+repr(self.titre) + "\n" +"\t"*(niveau+1)+ repr(self.feuille)+"\n"
        for fils in self.fils:
            retour += fils.__repr__(niveau+1)
        return retour

    def hasher(self):
        #Pour chaque feuille on récupére les hashtags
        ensHash = []
        for i in range(len(self.feuille)):
            ensHash+=re.findall("-(\w*)", self.feuille[i])

        self.hashtags = list(set(ensHash)) #Passer par le set permet de rendre les éléments uniques (la grosse flemme de faire un dico pour ça ...)
        return self.hashtags

#On lit le fichier:
#Ceci sont des lignes de test

def comptageDiese(ligne):
    """
    Compte le nombre de # en début de ligne
    """
    c = 0
    if len(ligne) == 0:
        return 0
    while ligne[c]=='#':
        c+=1
    return c


f = open('essai.md')
txt = f.read().split("\n")
f.close()

noeudCourant =  Noeud(txt[0], None) #On commence par la racine, qui n'a pas de père

nbDiese = 1                       # C'est un titre, donc on est en h1
hauteurTitreCourante = 1

for ligne in txt[1::]:                 # Puisque grâce au split, chaque indice de txt correspond à la i-ieme ligne...
    nbDieseCourant = comptageDiese(ligne)

    #Si c'est un paragraphe:
    #On ajoute dans le noeud courant

    if nbDieseCourant == 0:
        if ligne != '':
            if noeudCourant.feuille== []:
                noeudCourant.ajouterFeuille(ligne)
            else:
                noeudCourant.feuille[-1]+=ligne
        if ligne == '': # and ligne d'après ne commence pas par '#'
            noeudCourant.ajouterFeuille("")

    #Sinon, deux cas de présente:

    elif nbDieseCourant == hauteurTitreCourante:#On se déplace horizontalement
        nouveauFils = Noeud(ligne, noeudCourant.pere)
        noeudCourant = noeudCourant.pere.ajouterFils(nouveauFils)

    elif nbDieseCourant > hauteurTitreCourante:
        #On cree un nouveau fils avec le titre qui va bien:
        nouveauFils = Noeud(ligne, noeudCourant)
        noeudCourant = noeudCourant.ajouterFils(nouveauFils)
        hauteurTitreCourante = nbDieseCourant

    #Sinon, il faut remonter du nombre de différence de dièse + 1
    else: #Alors, nbDieseCourant < hauteurTitreCourante, c'est donc un saut, il faut remonter !
            # + 2 car +1 à cause du fait qu'il faut remonter une fois de plus, et +1 à cause du range
        for i in range(hauteurTitreCourante - nbDieseCourant + 2):
            noeudCourant = noeudCourant.getPere()

        nouveauNoeud = Noeud(ligne, pere=noeudCourant)
        noeudCourant = noeudCourant.ajouterFils(nouveauNoeud)
        hauteurTitreCourante=nbDieseCourant

    nbDiese = nbDieseCourant

#On remonte:

while noeudCourant.pere != None:
     noeudCourant = noeudCourant.pere

#On affiche
print noeudCourant.hasher()
